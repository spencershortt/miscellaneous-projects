//Spencer Shortt

//EXTREME Tic Tac Toe


//This code is a bit long and messy by itself. It also doesn't run without the touch sensors.
//For a video demonstration, please free to contact me at spencer.shortt@bobcats.gcsu.edu


/*Please add the  following academic honesty as comments, and insert it to the top of each of your programs:

 

Dr. Phelps/Liu has allowed students to discuss the algorithm and/or general concepts about the assignment with other students and to receive limited help on specific topics. However, I assume full responsibility for the content and integrity of this assignment. I have developed my own solution to this assigned project. I have not used or copied (by any means) another’s work (or portions of it) in order to represent it as my own including material from the internet. If I used a common computer, I have remembered to delete the files and empty the recycle bin. I have destroyed all extra printouts of my code. I have not shared my code with anyone. I am the sole author of the assignment; however,

I received outside help from the following people:*/


import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.image.ImageView;
import javafx.scene.image.Image;

import javafx.scene.shape.Sphere;
import javafx.stage.Stage;
import javafx.scene.control.*;
import javafx.event.*;
import java.io.*;
import java.util.Random; 
import javafx.scene.paint.*;
import javafx.scene.control.TextField;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.stage.Stage;
import javafx.geometry.Insets; 
import javafx.application.Platform;
import com.phidget22.*;
import javafx.scene.layout.*;
import javafx.scene.control.Label;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.text.*; 
import javafx.collections.ObservableList;
import javafx.scene.text.Font;
import javafx.scene.paint.Color;
public class spencerPhidgets extends Application implements VoltageRatioInputVoltageRatioChangeListener

{    
   private boolean b;
   
   private boolean turn; //True==X False==O
   
   public boolean[][] vacant=new boolean[9][9];
   public int[][] tracker=new int[9][9];
   
   
   
   private VoltageRatioInput sensor0;
   private VoltageRatioInput sensor1;
   private VoltageRatioInput sensor2;
   private VoltageRatioInput sensor3;
   private VoltageRatioInput sensor4;
   private VoltageRatioInput sensor5;
   private VoltageRatioInput sensor6;
   private VoltageRatioInput sensor7;
   private VoltageRatioInput sensor8;
   
   
   
   public int test=0;
   
   

        
   private Line row1=new Line();
   private Line row2=new Line();
   private Line row3=new Line();
   private Line row4=new Line();
   private Line row5=new Line();
   private Line row6=new Line();
   private Line row7=new Line();
   private Line row8=new Line();
   
   private Line col1=new Line();
   private Line col2=new Line();
   private Line col3=new Line();
   private Line col4=new Line();
   private Line col5=new Line();
   private Line col6=new Line();
   private Line col7=new Line();
   private Line col8=new Line();
   
   
   private int[] R;
   private int[] C;
   private int r;
   private int c;
   private int A;
   private int B;
   private boolean H;
   private int move;
   private int heldMove;
   private int[] p;
   private int[] q;
   
   private int[] squareStatus=new int[9];
   
   private Line[][] X1=new Line[9][9];
   private Line[][] X2=new Line[9][9];
   
   private Line[][] bigX1=new Line[9][9];
   private Line[][] bigX2=new Line[9][9];
   private Circle[][] bigO=new Circle[9][9];
   
   private Circle[][] O=new Circle[9][9];
   
   private Line cross1=new Line();
   private Line cross2=new Line();
   
   private Line light1=new Line();
   private Line light2=new Line();
   private Line light3=new Line();
   private Line light4=new Line();
   
   
   
   
   private VBox vb = new VBox();
   
   
   private Scene scene;
   private Group root;
     
   private TextField minutesField;
    
    
   private boolean Box;
   private Rectangle rec=new Rectangle();
   private VBox topLevelBox;
   private RadioButton[] rBs;
   private Button showSelectionButton;
    
    
   private int h;
   private boolean y;
    
   private boolean on=false;
   private DigitalOutput led1;
   private DigitalOutput led2;
   private ImageView imgView;
   private Image[] images=new Image[1];
    
   private int IMAGE;
   private VBox IBox;
    
    
   private final int FONTSIZE=15;
   public void start(Stage stage)throws Exception
   {
   
      root = new Group();
      scene = new Scene(root, 700, 700); 
        
        
      ToggleGroup rg=new ToggleGroup();
        
      VBox radiosBox = new VBox(10);
      radiosBox.setPadding(new Insets(scene.getHeight()/3, scene.getHeight()/3, 0, 0)); 
        
      rBs=new RadioButton[9];
      String [] optionLabels = 
         {"0",
         "1",
         "2","3","4","5","6","7","8"};
   
       //step3: Use a loop to create three RadioButton object 
       //and add them to the ToggleGroup rg
      for(int i=0; i<9; i++)
      {
         rBs[i]=new RadioButton(optionLabels[i]);
         rBs[i].setToggleGroup(rg);
      }
      Button fB = new Button("Choose Starting Board");
      radiosBox.getChildren().addAll(rBs);
        
      radiosBox.getChildren().addAll(fB);
        
        
        
        
        
        
        
        
      IMAGE=0;
      Box=false;//useless
      FileInputStream inputstream;
        // Create a pane.
      IBox = new VBox();   
      IBox.setAlignment(Pos.CENTER);
      Box=false;
      
      
      try{
         String imgName="areYouReady.jpg";
         inputstream = new FileInputStream(imgName);
         images[0] = new Image(imgName); 
      
         imgView=new ImageView();
         imgView.setImage(images[0]);
      }
      catch (Exception e)
      {
         System.out.println("File IO exception!");
      }
      IBox.getChildren().addAll(imgView);
      
      
      
      
         
         
         
      b=false;
      A=0;
      B=0;
      H=true;
        
        
      R=new int[] {0,1,2};//R tells you the row indices 
      C=new int[] {0,1,2};//C tells you the column indices
      p=new int[] {0,1,2};//for holding rows
      q=new int[] {0,1,2};//for holding columns
        
      move=0;//begin in the first box
        
       
             
        // Construct the top-level vertical box.             
      topLevelBox = new VBox(10);
        //HBox for minuteLabel and minutesFields
        
      Label minutesLabel = new Label("Choose next board:");
      minutesLabel.setFont(Font.font ("Verdana", FONTSIZE));
       
        
      minutesField=new TextField();
      HBox nBox=new HBox(10);
      nBox.getChildren().addAll(minutesLabel);
         
      HBox mBox=new HBox(10);
      mBox.getChildren().addAll(minutesField);
      nBox.setAlignment(Pos.CENTER); 
      mBox.setAlignment(Pos.CENTER); 
   
      topLevelBox.setAlignment(Pos.CENTER);     
      topLevelBox.setPadding(new Insets(3*scene.getHeight()/9, 3*scene.getHeight()/9, 3*scene.getHeight()/9, 3*scene.getHeight()/9));        
        
        // Create the button and selectionlabel
        // and add to the top-level box.
      showSelectionButton = new Button("Choose!");
      Label selectionLabel = new Label();       
      topLevelBox.getChildren().
         addAll(radiosBox, nBox, mBox, showSelectionButton, selectionLabel);
        
        
      
      //Make the white rectangle
      
      rec.setX(3*scene.getHeight()/9);
      rec.setY(3*scene.getHeight()/9);
      rec.setWidth(3.1*scene.getHeight()/9);
      rec.setHeight(3*scene.getHeight()/9);
      rec.setArcWidth(20);
      rec.setArcHeight(20);
      rec.setStroke(Color.BLACK);
      rec.setFill(Color.WHITE);
       
              
        //Make the lines
      row1.setStartX(0);
      row1.setEndX(scene.getHeight());
      row1.setStartY(scene.getHeight()/9);
      row1.setEndY(scene.getHeight()/9);
      ;
        
      row2.setStartX(0);
      row2.setEndX(scene.getHeight());
      row2.setStartY(2*scene.getHeight()/9);
      row2.setEndY(2*scene.getHeight()/9);
        
      row3.setStartX(0);
      row3.setEndX(scene.getHeight());
      row3.setStartY(3*scene.getHeight()/9);
      row3.setEndY(3*scene.getHeight()/9);
      row3.setStrokeWidth(10.0);
        
        
      row4.setStartX(0);
      row4.setEndX(scene.getHeight());
      row4.setStartY(4*scene.getHeight()/9);
      row4.setEndY(4*scene.getHeight()/9);
        
      row5.setStartX(0);
      row5.setEndX(scene.getHeight());
      row5.setStartY(5*scene.getHeight()/9);
      row5.setEndY(5*scene.getHeight()/9);
        
      row6.setStartX(0);
      row6.setEndX(scene.getHeight());
      row6.setStartY(6*scene.getHeight()/9);
      row6.setEndY(6*scene.getHeight()/9);
      row6.setStrokeWidth(10.0);
        
      row7.setStartX(0);
      row7.setEndX(scene.getHeight());
      row7.setStartY(7*scene.getHeight()/9);
      row7.setEndY(7*scene.getHeight()/9);
        
      row8.setStartX(0);
      row8.setEndX(scene.getHeight());
      row8.setStartY(8*scene.getHeight()/9);
      row8.setEndY(8*scene.getHeight()/9);
        
        
      col1.setStartX(scene.getHeight()/9);
      col1.setEndX(scene.getHeight()/9);
      col1.setStartY(0);
      col1.setEndY(scene.getHeight());
        
      col2.setStartX(2*scene.getHeight()/9);
      col2.setEndX(2*scene.getHeight()/9);
      col2.setStartY(0);
      col2.setEndY(scene.getHeight());
        
      col3.setStartX(3*scene.getHeight()/9);
      col3.setEndX(3*scene.getHeight()/9);
      col3.setStartY(0);
      col3.setEndY(scene.getHeight());
      col3.setStrokeWidth(10.0);
        
        
      col4.setStartX(4*scene.getHeight()/9);
      col4.setEndX(4*scene.getHeight()/9);
      col4.setStartY(0);
      col4.setEndY(scene.getHeight());
        
        
      col5.setStartX(5*scene.getHeight()/9);
      col5.setEndX(5*scene.getHeight()/9);
      col5.setStartY(0);
      col5.setEndY(scene.getHeight());
        
      col6.setStartX(6*scene.getHeight()/9);
      col6.setEndX(6*scene.getHeight()/9);
      col6.setStartY(0);
      col6.setEndY(scene.getHeight());       
      col6.setStrokeWidth(10.0);
        
      col7.setStartX(7*scene.getHeight()/9);
      col7.setEndX(7*scene.getHeight()/9);
      col7.setStartY(0);
      col7.setEndY(scene.getHeight());
        
      col8.setStartX(8*scene.getHeight()/9);
      col8.setEndX(8*scene.getHeight()/9);
      col8.setStartY(0);
      col8.setEndY(scene.getHeight());
      
      
      //For the touch sensors
      sensor0 = new VoltageRatioInput();
      sensor0.setDeviceSerialNumber(149232); //what is your serial number?
      sensor0.setChannel(0); //which channel did you add the sensor0?
      sensor0.open(5000);  
      sensor0.setVoltageRatioChangeTrigger(0.4);    
      sensor0.addVoltageRatioChangeListener(this);
      
      sensor1 = new VoltageRatioInput();
      sensor1.setDeviceSerialNumber(149232); //what is your serial number?
      sensor1.setChannel(1); //which channel did you add the sensor0?
      sensor1.open(5000);  
      sensor1.setVoltageRatioChangeTrigger(0.4);    
      sensor1.addVoltageRatioChangeListener(this);
      
      sensor2 = new VoltageRatioInput();
      sensor2.setDeviceSerialNumber(149232); //what is your serial number?
      sensor2.setChannel(2); //which channel did you add the sensor0?
      sensor2.open(5000);  
      sensor2.setVoltageRatioChangeTrigger(0.4);    
      sensor2.addVoltageRatioChangeListener(this);
      
      sensor3 = new VoltageRatioInput();
      sensor3.setDeviceSerialNumber(149232); //what is your serial number?
      sensor3.setChannel(3); //which channel did you add the sensor0?
      sensor3.open(5000);  
      sensor3.setVoltageRatioChangeTrigger(0.4);    
      sensor3.addVoltageRatioChangeListener(this);
      
      sensor4 = new VoltageRatioInput();
      sensor4.setDeviceSerialNumber(149232); //what is your serial number?
      sensor4.setChannel(4); //which channel did you add the sensor0?
      sensor4.open(5000);  
      sensor4.setVoltageRatioChangeTrigger(0.4);    
      sensor4.addVoltageRatioChangeListener(this);
      
      sensor5 = new VoltageRatioInput();
      sensor5.setDeviceSerialNumber(149232); //what is your serial number?
      sensor5.setChannel(5); //which channel did you add the sensor0?
      sensor5.open(5000);  
      sensor5.setVoltageRatioChangeTrigger(0.4);    
      sensor5.addVoltageRatioChangeListener(this);
      
      sensor6 = new VoltageRatioInput();
      sensor6.setDeviceSerialNumber(149232); //what is your serial number?
      sensor6.setChannel(6); //which channel did you add the sensor0?
      sensor6.open(5000);  
      sensor6.setVoltageRatioChangeTrigger(0.4);    
      sensor6.addVoltageRatioChangeListener(this);
      
      sensor7 = new VoltageRatioInput();
      sensor7.setDeviceSerialNumber(149232); //what is your serial number?
      sensor7.setChannel(7); //which channel did you add the sensor0?
      sensor7.open(5000);  
      sensor7.setVoltageRatioChangeTrigger(0.4);    
      sensor7.addVoltageRatioChangeListener(this);
      
      sensor8 = new VoltageRatioInput();
      sensor8.setDeviceSerialNumber(39301); //what is your serial number?
      sensor8.setChannel(2); //which channel did you add the sensor0?
      sensor8.open(5000);  
      sensor8.setVoltageRatioChangeTrigger(0.4);    
      sensor8.addVoltageRatioChangeListener(this);
      
      
         //LEDs
   
      led1 = new DigitalOutput();
      led1.open(5000);
      led1.setDutyCycle(0);
     
      led2 = new DigitalOutput();
      led2.open(5000);
      led2.setDutyCycle(0);
      
      
      
      //Add everyting to panel-- Make the board
      root.getChildren().add(row1);
      root.getChildren().add(row2);
      root.getChildren().add(row3);
      root.getChildren().add(row4);
      root.getChildren().add(row5);
      root.getChildren().add(row6);
      root.getChildren().add(row7);
      root.getChildren().add(row8);
      
      root.getChildren().add(col1);
      root.getChildren().add(col2);
      root.getChildren().add(col3);
      root.getChildren().add(col4);
      root.getChildren().add(col5);
      root.getChildren().add(col6);
      root.getChildren().add(col7);
      root.getChildren().add(col8);
     
     
      //Make lines that indicate the square being played in
      
      light1.setStartX(0);
      light1.setEndX(0);    
      light1.setStartY(0);
      light1.setEndY(0);
      
      light2.setStartX(0);
      light2.setEndX(0);    
      light2.setStartY(0);
      light2.setEndY(0);
      
      light3.setStartX(0);
      light3.setEndX(0);    
      light3.setStartY(0);
      light3.setEndY(0);
      
      light4.setStartX(0);
      light4.setEndX(0);    
      light4.setStartY(0);
      light4.setEndY(0);
      
      root.getChildren().add(light1);
      root.getChildren().add(light2);
      root.getChildren().add(light3);
      root.getChildren().add(light4);
      
      //Set all the Xs and Os to nothing
      for(r=0; r<9; r++)
      {
         for(c=0; c<9; c++)
         {
            X1[r][c]=new Line();
            X1[r][c].setStartX(0);
            X1[r][c].setEndX(0);    
            X1[r][c].setStartY(0);
            X1[r][c].setEndY(0);
            root.getChildren().add(X1[r][c]);
            
            X2[r][c]=new Line();
            X2[r][c].setStartX(0);
            X2[r][c].setEndX(1);    
            X2[r][c].setStartY(0);
            X2[r][c].setEndY(1);
            root.getChildren().add(X2[r][c]);
            
            O[r][c]=new Circle(0,0,0);
            root.getChildren().add(O[r][c]);
            
            vacant[r][c]=true;
            
            bigX1[r][c]=new Line();
            bigX1[r][c].setStartX(0);
            bigX1[r][c].setEndX(0);    
            bigX1[r][c].setStartY(0);
            bigX1[r][c].setEndY(0);
            root.getChildren().add(bigX1[r][c]);
            
            bigX2[r][c]=new Line();
            bigX2[r][c].setStartX(0);
            bigX2[r][c].setEndX(1);    
            bigX2[r][c].setStartY(0);
            bigX2[r][c].setEndY(1);
            root.getChildren().add(bigX2[r][c]);
            
            bigO[r][c]=new Circle(0,0,0);
            root.getChildren().add(bigO[r][c]);
            
            tracker[r][c]=0;//Tracks whether a square is full
            
            squareStatus[r]=0;//tracks whether a board is one
            
         }
      }
      
      
      
      
      try{
      
         //update based on touch sensors
         
         root.getChildren().add(X1[r][c]);
         root.getChildren().add(X2[r][c]);
         
         root.getChildren().add(bigX1[r][c]);
         root.getChildren().add(bigX2[r][c]);
        
         root.getChildren().add(bigO[r][c]);
         
         root.getChildren().add(light1);
         root.getChildren().add(light2);
         root.getChildren().add(light3);
         root.getChildren().add(light4);
         
         
         
        
      }
      catch (Exception e)
      {
         System.out.println("File IO exception!");
      }
      Button next=new Button("Next");
      EventHandler<ActionEvent> hAndler=
         event->
         {
           
         
         
         
           
         };
      next.setOnAction(hAndler);
        
       
       
      
      root.getChildren().addAll(radiosBox);
      // Set the handler for the show selection button.
      EventHandler<ActionEvent> handler = 
         event -> {
            
            
            
           
                  
                  
                  
            turn=!turn;//flip the turn
            boxLight(Integer.parseInt((minutesField.getText().trim())));//light up the chosen box
               
               
            turn=!turn;//flip the turn again
               
              
            h=Integer.parseInt((minutesField.getText().trim()));//get the integer chosen
            System.out.println("h: "+h);
            R=getNewRow(h);//Make the new Row index based off of chosen board 
            C=getNewCol(h);//Make the new Col index based off of chosen board 
            y=true;//Statement for helping determine keepMove
               
               
               
               
            root.getChildren().remove(rec);//get rid of box
            root.getChildren().remove(topLevelBox);//get rid of box
               
               
               
           
               
               
               
               
               
               
              
               
              
               
         };        
      showSelectionButton.setOnAction(handler);
        
      EventHandler<ActionEvent> Handler = 
         event -> {
         
            for(int z=0; z<rBs.length; z++)
            {
               if(rBs[z].isSelected())
               
               {
               
                  h=z;
                  R=getNewRow(h);//Make the new Row index based off of chosen board 
                  C=getNewCol(h);
                  y=true;
                  root.getChildren().remove(radiosBox);
               
               }
              
              
            }
         
         }; 
      fB.setOnAction(Handler);
      
    
       
      stage.setScene(scene);
       
      stage.show();
   }
    
    
   public void onVoltageRatioChange(VoltageRatioInputVoltageRatioChangeEvent e) 
   {
      try
      {
         
        
         Platform.runLater(
            //function without name, (), "->" body           
            () -> {            
               if(e.getVoltageRatio()>0.4)
               {
               
                  if(IMAGE==0)
                  {root.getChildren().addAll(IBox);}
                  if(IMAGE==1)
                  {root.getChildren().remove(IBox);}
                  IMAGE++;
               
               
               
                  heldMove=move;//hold move so that if board is won, the correct squareStatus is applied
               
                  if(y==true)//button was just pressed
                  {
                     heldMove=h;//if button was just pressed, then h indicates the board chosen.
                  }
                  y=false;//button unpressed
               
                  p=R;//hold Row
                  q=C;//hold Col
                  try{
                     if(turn==true){
                        led1.setDutyCycle(1);
                        led2.setDutyCycle(0);
                     }
                     if(turn==false){
                        led1.setDutyCycle(0);
                        led2.setDutyCycle(1);
                     }
                  }
                  catch (Exception ex)
                  {
                     ex.printStackTrace();
                  }
               
               
               
               
               
               
                  if(e.getSource() == sensor0)
                  {
                     move =0;//move equals new touch, indicates where the X or O should be placed, 
                       //and next board played on  
                  
                     if(vacant[getPresentRow(move, R)][getPresentCol(move, C)]==true)
                     {
                        boxLight(move);//lights up next board to be played on
                     
                     
                     
                     }
                  
                                 
                  
                  }
                  if(e.getSource() == sensor1)
                  {
                     move =1;
                     if(vacant[getPresentRow(move, R)][getPresentCol(move, C)]==true)
                     {
                        boxLight(move);
                     
                     
                                 
                     }
                  
                  
                  
                  
                  
                  
                  }
                  if(e.getSource() == sensor2)
                  {
                     move =2;
                     if(vacant[getPresentRow(move, R)][getPresentCol(move, C)]==true)
                     {boxLight(move);
                                 
                                
                     
                     }
                  }
                  if(e.getSource() == sensor3)
                  {
                     move =3;
                     if(vacant[getPresentRow(move, R)][getPresentCol(move, C)]==true)
                     {boxLight(move);
                     }
                  
                  }
                  if(e.getSource() == sensor4)
                  {
                     move =4;
                  
                     if(vacant[getPresentRow(move, R)][getPresentCol(move, C)]==true)
                     {boxLight(move);
                     
                     }
                  }
                  if(e.getSource() == sensor5)
                  {
                     move =5;
                  
                     if(vacant[getPresentRow(move, R)][getPresentCol(move, C)]==true)
                     {boxLight(move);
                     
                     }
                  }
                  if(e.getSource() == sensor6)
                  {
                     move =6;
                     if(vacant[getPresentRow(move, R)][getPresentCol(move, C)]==true)
                     {
                        boxLight(move);            }
                  }
                  if(e.getSource() == sensor7)
                  {
                     move =7;
                     if(vacant[getPresentRow(move, R)][getPresentCol(move, C)]==true)
                     {boxLight(move);
                     }
                  
                  
                  }
                  if(e.getSource() == sensor8)
                  {
                     move =8;
                     if(vacant[getPresentRow(move, R)][getPresentCol(move, C)]==true)
                     {boxLight(move);
                     
                     }
                  }
               
                  System.out.println("Move: "+move);
               
               
                  A=r;//hold exact previous row index played
                  B=c;//hold exact previous col index played
               
                  r=getPresentRow(move, R);//indicates row index just played
               
                  System.out.println("Present Row:"+r);
               
               
               
               
               
               
                  c=getPresentCol(move, C);//indicates col index just played
               
                  H=vacant[r][c];//whether the square is free or not
                  System.out.println("Present Col:"+c);
               
                  System.out.println("Turn:"+turn);
               
               //turn== true indicates it is X's turn
                  if(turn==true && vacant[r][c]==true)//if it is X's turn and the square is available 
                  {
                  
                  //draws X on (r,c)
                  
                     X1[r][c].setStartX((c+.1)*scene.getHeight()/9);
                     X1[r][c].setEndX((c+.9)*scene.getHeight()/9);
                     X1[r][c].setStartY((r+.1)*scene.getHeight()/9);
                     X1[r][c].setEndY((r+.9)*scene.getHeight()/9);
                     X1[r][c].setStrokeWidth(5.0);
                     X1[r][c].setStroke(Color.RED);
                  
                     X2[r][c].setStartX((c+.9)*scene.getHeight()/9);
                     X2[r][c].setEndX((c+.1)*scene.getHeight()/9);
                     X2[r][c].setStartY((r+.1)*scene.getHeight()/9);
                     X2[r][c].setEndY((r+.9)*scene.getHeight()/9);
                     X2[r][c].setStrokeWidth(5.0);
                     X2[r][c].setStroke(Color.RED);
                  
                     vacant[r][c]=false;//spot now taken
                  
                     tracker[r][c]=1;//indicates an X in this position
                     turn=!turn;//change turn
                  
                  }
                  else if(turn==false && vacant[r][c]==true)//same thing but with circle
                  {
                  //draws O on spot
                  
                     O[r][c].setCenterX((c+0.5)*scene.getHeight()/9); 
                     O[r][c].setCenterY((r+0.5)*scene.getHeight()/9); 
                     O[r][c].setRadius((0.4)*scene.getHeight()/9);
                     O[r][c].setFill(Color.WHITE);
                     O[r][c].setStroke(Color.BLUE);
                     O[r][c].setStrokeWidth(5.0);
                     vacant[r][c]=false;
                  
                     tracker[r][c]=2;
                     turn=!turn;
                  }
                  else
                  {
                     System.out.println("Try Again");
                  
                  }
               
               //turn=!turn;
                  System.out.println("New Turn:"+ turn);
               
                  if(boardCheck(tracker, R, C)==1)//if there are three in a row not including diagonal
                  {
                  
                  //Draw large X
                     System.out.println("hi_1");
                     bigX1[r][c].setStartX(C[0]*scene.getHeight()/9);
                     bigX1[r][c].setEndX((C[2]+1)*scene.getHeight()/9);    
                     bigX1[r][c].setStartY(R[0]*scene.getHeight()/9);
                     bigX1[r][c].setEndY((R[2]+1)*scene.getHeight()/9);
                     bigX1[r][c].setStrokeWidth(10.0);
                     bigX1[r][c].setStroke(Color.RED);
                  
                     bigX2[r][c].setStartX((C[2]+1)*scene.getHeight()/9);
                     bigX2[r][c].setEndX(C[0]*scene.getHeight()/9);    
                     bigX2[r][c].setStartY(R[0]*scene.getHeight()/9);
                     bigX2[r][c].setEndY((R[2]+1)*scene.getHeight()/9);
                     bigX2[r][c].setStrokeWidth(10.0);
                     bigX2[r][c].setStroke(Color.RED);
                  
                  
                  //deletes everything else in the board
                     for(int j=0; j<3; j++)
                     {
                        for(int v=0; v<3; v++)
                        {
                           root.getChildren().remove(O[R[j]][C[v]]);
                           root.getChildren().remove(X1[R[j]][C[v]]);
                           root.getChildren().remove(X2[R[j]][C[v]]);
                        }
                     }
                  
                  
                     squareStatus[heldMove]=1;//board has been won. Uses heldMove because that is the board index, 
                                        //while move indicates where the X is drawn and the next board played
                  
                  }
               
               
               
               
               
                  if(boardCheck(tracker, R, C)==2)
                  {
                  //same thing but with O
                     System.out.println("hi_1");
                     bigO[r][c].setCenterX((C[1]+0.5)*scene.getHeight()/9); 
                     bigO[r][c].setCenterY((R[1]+0.5)*scene.getHeight()/9); 
                     bigO[r][c].setRadius(scene.getHeight()/6);
                     bigO[r][c].setFill(Color.WHITE);
                     bigO[r][c].setStroke(Color.BLUE);
                     bigO[r][c].setStrokeWidth(10.0);
                  
                     for(int j=0; j<3; j++)
                     {
                        for(int v=0; v<3; v++)
                        {
                           root.getChildren().remove(O[R[j]][C[v]]);
                           root.getChildren().remove(X1[R[j]][C[v]]);
                           root.getChildren().remove(X2[R[j]][C[v]]);
                        }
                     }
                     squareStatus[heldMove]=1;//game has been won already
                  
                  
                  }
               
               
                  System.out.println("heldMove: "+heldMove);
                  System.out.println("squareStatus[heldMove]: "+squareStatus[heldMove]);
               //Recently added H==true statements
                  if(H==true && squareStatus[move]==0)//if the next board has not been won
                  {
                     R=getNewRow(move);//get the Row indices of the next box
                     C=getNewCol(move);//get the Col indices of the next box
                  
                  }
                  Random rand=new Random();//useless
                  int k=rand.nextInt(9);//useless
               
                  System.out.println("squareStatus[move]: "+squareStatus[move]); 
               
                  if(H==true && squareStatus[move]==1)//if the next board has been won already
                  {
                  
                  // Box=true;//useless
                  
                  
                  //Make the Choosing Box appear at the next board
                     if(move==0 || move==1 || move==2)
                     {
                     
                     
                        rec.setX(move*scene.getHeight()/3);
                        rec.setY(0);
                        rec.setWidth(3.1*scene.getHeight()/9);
                        rec.setHeight(3*scene.getHeight()/9);
                        rec.setArcWidth(20);
                        rec.setArcHeight(20);
                        rec.setStroke(Color.BLACK);
                        rec.setFill(Color.WHITE);
                     
                     
                        topLevelBox.setPadding(new Insets(0, 0, 2*scene.getHeight()/3, move*scene.getHeight()/3));        
                     
                     
                     }
                     if(move==3 || move==4 || move==5)
                     {
                     
                     
                        rec.setX((move-3)*scene.getHeight()/3);
                        rec.setY(scene.getHeight()/3);
                        rec.setWidth(3.1*scene.getHeight()/9);
                        rec.setHeight(3*scene.getHeight()/9);
                        rec.setArcWidth(20);
                        rec.setArcHeight(20);
                        rec.setStroke(Color.BLACK);
                        rec.setFill(Color.WHITE);
                     
                        topLevelBox.setPadding(new Insets(scene.getHeight()/3, 0, scene.getHeight()/3, (move-3)*scene.getHeight()/3));
                     }
                  //else
                     if(move==6 || move==7 || move==8)
                     {
                     
                     
                        rec.setX((move-6)*scene.getHeight()/3);
                        rec.setY(2*scene.getHeight()/3);
                        rec.setWidth(3.1*scene.getHeight()/9);
                        rec.setHeight(3*scene.getHeight()/9);
                        rec.setArcWidth(20);
                        rec.setArcHeight(20);
                        rec.setStroke(Color.BLACK);
                        rec.setFill(Color.WHITE);
                        topLevelBox.setPadding(new Insets(2*scene.getHeight()/3, 0, 0, (move-6)*scene.getHeight()/3));
                     }
                  
                  
                  //  System.out.println("Box: "+Box);
                     root.getChildren().add(rec);//add the box
                     root.getChildren().add(topLevelBox);//add the box
                  //turn=!turn;
                  }
               
                  if(H==false && squareStatus[move]==0)//if square from move is not vacant and if square is not won
                  {
                     R=p;
                     C=q;
                  //boxLight(heldMove);
                  }
               
               
               
               
               }          
                                             
            });             
          
      }
      catch (Exception ex)     
      {        
         ex.printStackTrace();
      }
   
      
     
   }
       
   public static int[] getNewRow(int m)
   {
      int[] Row={0,0,0};
      if(m==0 || m==1 || m==2)
      { 
         Row=new int[] {0,1,2};
      }
      else if(m==3 || m==4 || m==5)
      { 
         Row=new int[] {3,4,5};
      }
      else if(m==6 || m==7 || m==8)
      { 
         Row=new int[] {6,7,8};
      }
      
      return Row;
   }
   public static int[] getNewCol(int m)
   {
      int[] Col={0,0,0};
      if(m==0 || m==3 || m==6)
      { 
         Col=new int[]{0,1,2};
      }
      
      
      else if(m==1 || m==4 || m==7)
      { 
         Col=new int[] {3,4,5};
      }
      else if(m==2 || m==5 || m==8)
      { 
         Col=new int[] {6,7,8};
      }
      return Col;
   }
  
   public static int getPresentRow(int m ,int[] Row)
   {
      int ROW=0;
      if(m==0 || m==1 || m==2)
      { 
         ROW=Row[0];
      }
      else if(m==3 || m==4 || m==5)
      { 
         ROW=Row[1];
      }
      else if(m==6 || m==7 || m==8)
      { 
         ROW=Row[2];
      }
      
      return ROW;
   
      
   }
   public static int getPresentCol(int m ,int[] Col)
   {
      int COL=0;
      if(m==0 || m==3 || m==6)
      { 
         COL=Col[0];
      }
      
      
      else if(m==1 || m==4 || m==7)
      { 
         COL=Col[1];
      }
      else if(m==2 || m==5 || m==8)
      { 
         COL=Col[2];
      }
      return COL;
   }
    
    
   public static int boardCheck(int[][] g, int[] Row, int[] Col)
   {
    //System.out.println("hi");
      int i; 
    
    
    
    
    
      for(i=0; i<3; i++)
      {
       
       
         if(g[Row[i]][Col[0]]!=0 && g[Row[i]][Col[0]]==g[Row[i]][Col[1]] && g[Row[i]][Col[1]]==g[Row[i]][Col[2]])
         {
            System.out.println("This should win by Row");
            System.out.println("Start:");
            System.out.println(g[Row[i]][Col[0]]);
            System.out.println(g[Row[i]][Col[1]]);
            System.out.println(g[Row[i]][Col[2]]);
            System.out.println("End.");
            return g[Row[i]][Col[0]];
         }
         if(g[Row[0]][Col[i]]!=0 && g[Row[0]][Col[i]]==g[Row[1]][Col[i]] && g[Row[1]][Col[i]]==g[Row[2]][Col[i]])
         {
            System.out.println("This should win by Column");
            System.out.println("Start:");
            System.out.println(g[Row[0]][Col[i]]);
            System.out.println(g[Row[1]][Col[i]]);
            System.out.println(g[Row[2]][Col[i]]);
            System.out.println("End.");
            return g[Row[0]][Col[i]];
         }
          
      }
      return 0;
   }
    
    
   public void boxLight(int m)
   {
    
      if(m==0)
      {
         light1.setStartX(0);
         light1.setEndX(scene.getHeight()/3);    
         light1.setStartY(scene.getHeight()/3);
         light1.setEndY(scene.getHeight()/3);
               
         light2.setStartX(scene.getHeight()/3);
         light2.setEndX(scene.getHeight()/3);    
         light2.setStartY(0);
         light2.setEndY(scene.getHeight()/3);
                  
         light3.setStartX(0);
         light3.setEndX(0);    
         light3.setStartY(0);
         light3.setEndY(0);
                  
         light4.setStartX(0);
         light4.setEndX(0);    
         light4.setStartY(0);
         light4.setEndY(0);
                  
                  
         if(turn==true)
         {
            light1.setStrokeWidth(10.0);
            light1.setStroke(Color.BLUE);
            light2.setStrokeWidth(10.0);
            light2.setStroke(Color.BLUE);
            light3.setStrokeWidth(10.0);
            light3.setStroke(Color.BLUE);
            light4.setStrokeWidth(10.0);
            light4.setStroke(Color.BLUE);
         }
         if(turn==false)
         {
            light1.setStrokeWidth(10.0);
            light1.setStroke(Color.RED);
            light2.setStrokeWidth(10.0);
            light2.setStroke(Color.RED);
            light3.setStrokeWidth(10.0);
            light3.setStroke(Color.RED);
            light4.setStrokeWidth(10.0);
            light4.setStroke(Color.RED);
         }
      
      
      }
      else if(m==1)
      {
         light1.setStartX(scene.getHeight()/3);
         light1.setEndX(2*scene.getHeight()/3);    
         light1.setStartY(scene.getHeight()/3);
         light1.setEndY(scene.getHeight()/3);
               
         light2.setStartX(scene.getHeight()/3);
         light2.setEndX(scene.getHeight()/3);    
         light2.setStartY(0);
         light2.setEndY(scene.getHeight()/3);
                  
         light3.setStartX(2*scene.getHeight()/3);
         light3.setEndX(2*scene.getHeight()/3);    
         light3.setStartY(0);
         light3.setEndY(scene.getHeight()/3);
                  
         light4.setStartX(0);
         light4.setEndX(0);    
         light4.setStartY(0);
         light4.setEndY(0);
                  
                  
         if(turn==true)
         {
            light1.setStrokeWidth(10.0);
            light1.setStroke(Color.BLUE);
            light2.setStrokeWidth(10.0);
            light2.setStroke(Color.BLUE);
            light3.setStrokeWidth(10.0);
            light3.setStroke(Color.BLUE);
            light4.setStrokeWidth(10.0);
            light4.setStroke(Color.BLUE);
         }
         if(turn==false)
         {
            light1.setStrokeWidth(10.0);
            light1.setStroke(Color.RED);
            light2.setStrokeWidth(10.0);
            light2.setStroke(Color.RED);
            light3.setStrokeWidth(10.0);
            light3.setStroke(Color.RED);
            light4.setStrokeWidth(10.0);
            light4.setStroke(Color.RED);
         }
      }
      else if(m==2)
      {
      
         light1.setStartX(2*scene.getHeight()/3);
         light1.setEndX(scene.getHeight());    
         light1.setStartY(scene.getHeight()/3);
         light1.setEndY(scene.getHeight()/3);
               
         light2.setStartX(0);
         light2.setEndX(0);    
         light2.setStartY(0);
         light2.setEndY(0);
                  
         light3.setStartX(2*scene.getHeight()/3);
         light3.setEndX(2*scene.getHeight()/3);    
         light3.setStartY(0);
         light3.setEndY(scene.getHeight()/3);
                  
         light4.setStartX(0);
         light4.setEndX(0);    
         light4.setStartY(0);
         light4.setEndY(0);
                  
                  
         if(turn==true)
         {
            light1.setStrokeWidth(10.0);
            light1.setStroke(Color.BLUE);
            light2.setStrokeWidth(10.0);
            light2.setStroke(Color.BLUE);
            light3.setStrokeWidth(10.0);
            light3.setStroke(Color.BLUE);
            light4.setStrokeWidth(10.0);
            light4.setStroke(Color.BLUE);
         }
         if(turn==false)
         {
            light1.setStrokeWidth(10.0);
            light1.setStroke(Color.RED);
            light2.setStrokeWidth(10.0);
            light2.setStroke(Color.RED);
            light3.setStrokeWidth(10.0);
            light3.setStroke(Color.RED);
            light4.setStrokeWidth(10.0);
            light4.setStroke(Color.RED);
         }
      
      }
      else if(m==3)
      {
         light1.setStartX(0);
         light1.setEndX(scene.getHeight()/3);    
         light1.setStartY(scene.getHeight()/3);
         light1.setEndY(scene.getHeight()/3);
               
         light2.setStartX(0);
         light2.setEndX(0);    
         light2.setStartY(0);
         light2.setEndY(0);
                  
         light3.setStartX(scene.getHeight()/3);
         light3.setEndX(scene.getHeight()/3);    
         light3.setStartY(scene.getHeight()/3);
         light3.setEndY(2*scene.getHeight()/3);
                  
         light4.setStartX(0);
         light4.setEndX(0);    
         light4.setStartY(0);
         light4.setEndY(0);
                  
                  
         if(turn==true)
         {
            light1.setStrokeWidth(10.0);
            light1.setStroke(Color.BLUE);
            light2.setStrokeWidth(10.0);
            light2.setStroke(Color.BLUE);
            light3.setStrokeWidth(10.0);
            light3.setStroke(Color.BLUE);
            light4.setStrokeWidth(10.0);
            light4.setStroke(Color.BLUE);
         }
         if(turn==false)
         {
            light1.setStrokeWidth(10.0);
            light1.setStroke(Color.RED);
            light2.setStrokeWidth(10.0);
            light2.setStroke(Color.RED);
            light3.setStrokeWidth(10.0);
            light3.setStroke(Color.RED);
            light4.setStrokeWidth(10.0);
            light4.setStroke(Color.RED);
         }
      
      
      
      }
      else if(m==4)
      {
         light1.setStartX(scene.getHeight()/3);
         light1.setEndX(2*scene.getHeight()/3);    
         light1.setStartY(scene.getHeight()/3);
         light1.setEndY(scene.getHeight()/3);
               
         light2.setStartX(scene.getHeight()/3);
         light2.setEndX(scene.getHeight()/3);    
         light2.setStartY(scene.getHeight()/3);
         light2.setEndY(2*scene.getHeight()/3);
                  
         light3.setStartX(2*scene.getHeight()/3);
         light3.setEndX(2*scene.getHeight()/3);    
         light3.setStartY(scene.getHeight()/3);
         light3.setEndY(2*scene.getHeight()/3);
                  
         light4.setStartX(scene.getHeight()/3);
         light4.setEndX(2*scene.getHeight()/3);    
         light4.setStartY(2*scene.getHeight()/3);
         light4.setEndY(2*scene.getHeight()/3);
                  
                  
         if(turn==true)
         {
            light1.setStrokeWidth(10.0);
            light1.setStroke(Color.BLUE);
            light2.setStrokeWidth(10.0);
            light2.setStroke(Color.BLUE);
            light3.setStrokeWidth(10.0);
            light3.setStroke(Color.BLUE);
            light4.setStrokeWidth(10.0);
            light4.setStroke(Color.BLUE);
         }
         if(turn==false)
         {
            light1.setStrokeWidth(10.0);
            light1.setStroke(Color.RED);
            light2.setStrokeWidth(10.0);
            light2.setStroke(Color.RED);
            light3.setStrokeWidth(10.0);
            light3.setStroke(Color.RED);
            light4.setStrokeWidth(10.0);
            light4.setStroke(Color.RED);
         }
      
      }
      else if(m==5)
      {
         light1.setStartX(2*scene.getHeight()/3);
         light1.setEndX(scene.getHeight());    
         light1.setStartY(scene.getHeight()/3);
         light1.setEndY(scene.getHeight()/3);
               
         light2.setStartX(2*scene.getHeight()/3);
         light2.setEndX(scene.getHeight());    
         light2.setStartY(2*scene.getHeight()/3);
         light2.setEndY(2*scene.getHeight()/3);
                  
         light3.setStartX(2*scene.getHeight()/3);
         light3.setEndX(2*scene.getHeight()/3);    
         light3.setStartY(scene.getHeight()/3);
         light3.setEndY(2*scene.getHeight()/3);
                  
         light4.setStartX(0);
         light4.setEndX(0);    
         light4.setStartY(0);
         light4.setEndY(0);
                  
                  
         if(turn==true)
         {
            light1.setStrokeWidth(10.0);
            light1.setStroke(Color.BLUE);
            light2.setStrokeWidth(10.0);
            light2.setStroke(Color.BLUE);
            light3.setStrokeWidth(10.0);
            light3.setStroke(Color.BLUE);
            light4.setStrokeWidth(10.0);
            light4.setStroke(Color.BLUE);
         }
         if(turn==false)
         {
            light1.setStrokeWidth(10.0);
            light1.setStroke(Color.RED);
            light2.setStrokeWidth(10.0);
            light2.setStroke(Color.RED);
            light3.setStrokeWidth(10.0);
            light3.setStroke(Color.RED);
            light4.setStrokeWidth(10.0);
            light4.setStroke(Color.RED);
         }
      
      }
      else if(m==6)
      {
         light1.setStartX(0);
         light1.setEndX(scene.getHeight()/3);    
         light1.setStartY(2*scene.getHeight()/3);
         light1.setEndY(2*scene.getHeight()/3);
               
         light2.setStartX(scene.getHeight()/3);
         light2.setEndX(scene.getHeight()/3);    
         light2.setStartY(2*scene.getHeight()/3);
         light2.setEndY(scene.getHeight());
                  
         light3.setStartX(0);
         light3.setEndX(0);    
         light3.setStartY(0);
         light3.setEndY(0);
                  
         light4.setStartX(0);
         light4.setEndX(0);    
         light4.setStartY(0);
         light4.setEndY(0);
                  
                  
         if(turn==true)
         {
            light1.setStrokeWidth(10.0);
            light1.setStroke(Color.BLUE);
            light2.setStrokeWidth(10.0);
            light2.setStroke(Color.BLUE);
            light3.setStrokeWidth(10.0);
            light3.setStroke(Color.BLUE);
            light4.setStrokeWidth(10.0);
            light4.setStroke(Color.BLUE);
         }
         if(turn==false)
         {
            light1.setStrokeWidth(10.0);
            light1.setStroke(Color.RED);
            light2.setStrokeWidth(10.0);
            light2.setStroke(Color.RED);
            light3.setStrokeWidth(10.0);
            light3.setStroke(Color.RED);
            light4.setStrokeWidth(10.0);
            light4.setStroke(Color.RED);
         }
      
      
      }
      else if(m==7)
      {
         light1.setStartX(scene.getHeight()/3);
         light1.setEndX(2*scene.getHeight()/3);    
         light1.setStartY(2*scene.getHeight()/3);
         light1.setEndY(2*scene.getHeight()/3);
               
         light2.setStartX(scene.getHeight()/3);
         light2.setEndX(scene.getHeight()/3);    
         light2.setStartY(2*scene.getHeight()/3);
         light2.setEndY(scene.getHeight());
                  
         light3.setStartX(2*scene.getHeight()/3);
         light3.setEndX(2*scene.getHeight()/3);    
         light3.setStartY(2*scene.getHeight()/3);
         light3.setEndY(scene.getHeight());
                  
         light4.setStartX(0);
         light4.setEndX(0);    
         light4.setStartY(0);
         light4.setEndY(0);
                  
                  
         if(turn==true)
         {
            light1.setStrokeWidth(10.0);
            light1.setStroke(Color.BLUE);
            light2.setStrokeWidth(10.0);
            light2.setStroke(Color.BLUE);
            light3.setStrokeWidth(10.0);
            light3.setStroke(Color.BLUE);
            light4.setStrokeWidth(10.0);
            light4.setStroke(Color.BLUE);
         }
         if(turn==false)
         {
            light1.setStrokeWidth(10.0);
            light1.setStroke(Color.RED);
            light2.setStrokeWidth(10.0);
            light2.setStroke(Color.RED);
            light3.setStrokeWidth(10.0);
            light3.setStroke(Color.RED);
            light4.setStrokeWidth(10.0);
            light4.setStroke(Color.RED);
         }
      
      
      }
      else if(m==8)
      {
         light1.setStartX(2*scene.getHeight()/3);
         light1.setEndX(scene.getHeight());    
         light1.setStartY(2*scene.getHeight()/3);
         light1.setEndY(2*scene.getHeight()/3);
               
         light2.setStartX(2*scene.getHeight()/3);
         light2.setEndX(2*scene.getHeight()/3);    
         light2.setStartY(2*scene.getHeight()/3);
         light2.setEndY(scene.getHeight());
                  
         light3.setStartX(0);
         light3.setEndX(0);    
         light3.setStartY(0);
         light3.setEndY(0);
                  
         light4.setStartX(0);
         light4.setEndX(0);    
         light4.setStartY(0);
         light4.setEndY(0);
                  
                  
         if(turn==true)
         {
            light1.setStrokeWidth(10.0);
            light1.setStroke(Color.BLUE);
            light2.setStrokeWidth(10.0);
            light2.setStroke(Color.BLUE);
            light3.setStrokeWidth(10.0);
            light3.setStroke(Color.BLUE);
            light4.setStrokeWidth(10.0);
            light4.setStroke(Color.BLUE);
         }
         if(turn==false)
         {
            light1.setStrokeWidth(10.0);
            light1.setStroke(Color.RED);
            light2.setStrokeWidth(10.0);
            light2.setStroke(Color.RED);
            light3.setStrokeWidth(10.0);
            light3.setStroke(Color.RED);
            light4.setStrokeWidth(10.0);
            light4.setStroke(Color.RED);
         }
      
      }
   }
  
    
   public static void main(String args[]){ 
      launch(args); 
   } 
}
