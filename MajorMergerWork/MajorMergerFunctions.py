import numpy as np

def test():
    print("Test Success")


def delta_k(k,allK):
    dk=np.absolute(k-allK)
    return dk 

def distance_2d(x,y,allX,allY):
    r=np.sqrt((x - allX) ** 2.0 + (y - allY) ** 2.0)  
    return r

def rel_v_z(v_z, allV_z):
    rel_v_z=np.absolute(v_z-allV_z)
    return rel_v_z


def mass_ratio(m, allM):
    mr=m/allM
    return mr

def distance_3d(x,y,z,allX,allY,allZ):
    r=np.sqrt((x - allX) ** 2.0 + (y - allY) ** 2.0 +(z - allZ) ** 2.0)  
    return r

def rel_v_3d(v_x,v_y,v_z,allV_x,allV_y,allV_z):
    rel_v=np.sqrt((v_x - allV_x) ** 2.0 + (v_y - allV_y) ** 2.0 +(v_z - allV_z) ** 2.0)  
    return rel_v